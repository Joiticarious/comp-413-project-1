package hw;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestHelloWorld {

	private HelloWorld fixture;
	
	@Before
	public void setUp() throws Exception {
		fixture = new HelloWorld();
	}

	@After
	public void tearDown() throws Exception {
		fixture = null;
	}

	@Test
	public void getMessage() {
		assertNotNull(fixture);
		assertEquals("hello world", fixture.getMessage());
	}

	@Test
	public void getMessage2() {
		assertNotNull(fixture); //changed this from assertNull to assertNotNull. assertNull states that an object is Null but in this case, fixture isn't Null.
		assertEquals("hello world", fixture.getMessage());
	}

	@Test
	public void getYear() {
		assertNotNull(fixture);
		assertEquals(2008, fixture.getYear()); //changed this from 2009 to 2008 because 2008 was the returned value in HellowWorld.java, hence, fixture.getYear() will always return 2008
	}

}
